# avr_tmpl

## How to use

- clone or download this repo

```bash
git clone https://gitlab.com/ru-avr/avr_tmpl.git <projec_name>
```

- remove `.git` directory
- change project name in README.md
- change project name in Makefile
