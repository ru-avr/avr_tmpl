#include <avr/io.h>
#include <util/delay.h>

#define LED_ON()     (PORTB |= (1 << PORT5))
#define LED_OFF()    (PORTB &= ~(1 << PORT5))
#define LED_TOGGLE() (PORTB ^= (1 << PORT5))

int main(void) {
    DDRB |= (1 << DD5);

    while (1) {
        LED_TOGGLE();
        _delay_ms(1000);
    }

    return 0;
}
