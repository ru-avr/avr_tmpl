TARGET = avr_tmpl

PREFIX = avr-

ifdef GCC_AVR_PATH
CC = $(GCC_AVR_PATH)/$(PREFIX)gcc
LD = $(GCC_AVR_PATH)/$(PREFIX)ld
AS = $(GCC_AVR_PATH)/$(PREFIX)gcc
CP = $(GCC_AVR_PATH)/$(PREFIX)objcopy
SZ = $(GCC_AVR_PATH)/$(PREFIX)size
DP = $(GCC_AVR_PATH)/$(PREFIX)objdump
else
CC = $(PREFIX)gcc
LD = $(PREFIX)ld
AS = $(PREFIX)gcc
CP = $(PREFIX)objcopy
SZ = $(PREFIX)size
DP = $(PREFIX)objdump
endif

AVRDUDE = /usr/bin/avrdude
RM = /usr/bin/rm -fR
MKDIR = /usr/bin/mkdir -p
ECHO = /usr/bin/echo -e

PROGRAMMER = avrisp2
MCU = atmega168pa

# definition for programmer
ifeq ($(MCU), atmega168pa)
MCU_PR = m168p 
else ifeq ($(MCU), atmega328p)
MCU_PR = m328p
else 
$(error "No programmer definition for $(MCU)")
endif

# AVR fuse calculator: http://www.engbedded.com/fusecalc
# default fuses values
LFUSE = 0x62
HFUSE = 0xDF
EFUSE = 0xF9

BUILD_DIR = ./build

# defines
C_DEFINES =  \
	F_CPU=1000000

C_INCDIRS = \
	/usr/avr/include/ \
	./src \
	./libs \

C_SOURCES = \
	./src/main.c \

# ru_avrlib
C_SOURCES += \

# ru_deviceslib
C_SOURCES += \

# include ./include.mk

C_DEFS := $(patsubst %,-D%, $(C_DEFINES))
C_INCLUDE := $(patsubst %,-I%, $(C_INCDIRS))

OBJS = $(C_SOURCES:.c=.o)
LIBSPEC = 
CFLAGS = $(LIBSPEC) -std=c99 -Wall -g -Os -mmcu=$(MCU) $(C_DEFS)

all: $(BUILD_DIR) $(OBJS)
	$(CC) $(CFLAGS) -Wl,-Map,$(BUILD_DIR)/$(TARGET).map -o $(BUILD_DIR)/$(TARGET).elf $(OBJS)
	$(CP) -j .text -j .data -O ihex $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex
	$(SZ) $(BUILD_DIR)/$(TARGET).elf

$(BUILD_DIR):
	$(MKDIR) $(BUILD_DIR)

%.o: %.c
	$(CC) $(CFLAGS) $(C_INCLUDE) -c $< -o $@

.c.s:
	$(CC) $(CFLAGS) -S $< -o $@

%.lst: %.elf
	$(DP) -d $< > $@

flash:
	$(AVRDUDE) -p $(MCU_PR) -c $(PROGRAMMER) -P usb -U flash:w:$(BUILD_DIR)/$(TARGET).hex:i -F

fuse_write:
	$(AVRDUDE) -p $(MCU_PR) -c $(PROGRAMMER) -P usb -U lfuse:w:$(LFUSE):m -U hfuse:w:$(HFUSE):m -U efuse:w:$(EFUSE):m

fuse_read:
	$(AVRDUDE) -p $(MCU_PR) -c $(PROGRAMMER) -P usb -U lfuse:r:-:h -U hfuse:r:-:h -U efuse:r:-:h

check_mcu:
	$(AVRDUDE) -p $(MCU_PR) -c $(PROGRAMMER) -P usb

clean:
	$(RM) $(OBJS)
	$(RM) $(BUILD_DIR)/$(TARGET)*
